
public class Krug extends Figura {
	private double radius;
	private final double pi = 3.1415; // constanta sozdana

	public Krug(double r) {
		radius = r;
	}

	@Override
	public String getImya() {
		return "Krug";
	}

	@Override
	public double getPloshyad() {
		return pi * (radius * radius);
	}
}
