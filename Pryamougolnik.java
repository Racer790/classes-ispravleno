
public class Pryamougolnik extends Figura {
	private double visota;
	private double shirina;

	public Pryamougolnik(double v, double s) {
		visota = v;
		shirina = s;
	}

	@Override
	public String getImya() {
		return "Pryamougolnik";
	}

	@Override
	public double getPloshyad() {
		return visota * shirina;
	}
}
